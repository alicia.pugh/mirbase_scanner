## miRBase scanner

This is a python-based script that scans the downloadable file from miRBase for a specific organism and outputs the sequence ID then the sequence into the console. It is recommended that you run this file in the shell and then direct the output to the file of your own choosing.

Currently, the script is for looking for A thaliana in a file named miRNA.dat in the same repository as the script. I just had to find the miRNA for that one organism, so you will have to edit the code to specify the organism you are looking for. Alternatively, there is commented code in there that was going to be the framework for running the program and specifying the filename and organism name as arguments. I may come back and edit that so it works later, but for now it does not.

