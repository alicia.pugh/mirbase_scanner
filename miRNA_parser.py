

import sys
with open("./miRNA.dat") as i:
#print(input)
     input = i.readlines()
#print(input)
species = "Arabidopsis thaliana"
add = False
endseq = True
#output = []

#read file line by line
#length = len(input)
for l in input: 
    if l.startswith("DE")==True: #if the line has the correct line marker
        if species in l: #and matches the correct species
            #output.append(l) #add miRNA description line to file
            print(">"+l)
            add = True #switch on printing for this line
        # else:
        #         output.append(".") #marker to check loop entry

    if add == True: #only print SQ of that species
        if l.startswith("SQ"):
             endseq = False
    
    if add == True and endseq == False and l.startswith("SQ")!= True:
        #output.append(l) #add the line to the output
        print(l)
        if l.startswith("//"):
            endseq = True
            add = False #switch printing back off for the next line
    #elif


#print(str(output)) #return search results



# def scan_file(filename, species):
#     import mmap
#     output = ""
#     marker = 0
#     with open(filename, 'r') as f:
#             mm = mmap.mmap(f.fileno(), 0)
#             while marker<4000001:
#                 line = mm.readline()
#                 if line.startswith("DE")==True:
#                     if line.find(species) == True:
#                         output.append(line+'\n')
#                 else:
#                      output.append("-")
#     return(output)

# print(scan_file("miRNA.dat", "Arabidopsis thaliana"))


